;   Voltage Reference - Put variable resistence to a LED
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of Voltage Reference
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;Voltage Reference (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the voltage reference value
; - RB1 - 7 - bit 1 of the voltage reference value
; - RB2 - 8 - bit 2 of the voltage reference value
; - RB3 - 9 - bit 3 of the voltage reference value
; - RB4 - 10 - voltage reference range
; - RA0 - 17 - switch of the voltage reference range
; - RA1 - 18 - switch of the voltage reference output values
; - RA2 - 1 - voltage reference output
;=============================

;Variables
;==============================
VALUE EQU 0x27
TEMP EQU 0x28

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start	

;Functions
;==============================
	include digitecnology.inc
ChangeVRefRange
	call chgbnk1
	btfss VRCON,5
	goto $+2
	goto $+3
	bsf VRCON,5
	goto $+2
	bcf VRCON,5
	return
ChangeVRefValue
	call chgbnk0
	incf VALUE,1 ;Increment value
	movf VALUE,0 ;Move VALUE to W
	sublw .16 ;Check if VALUE = 16
	btfsc STATUS,2 ;Check if arithmetic operation was zero
	clrf VALUE ;Clear VALUE
	call chgbnk1
	;Put VALUE into voltage reference
	movf VRCON,0 ;Move VRCON to W
        andlw b'11110000' ;Prepare VRCON<0:3> for changing bits
	movwf TEMP
	call chgbnk0
	movf VALUE,0
	andlw b'00001111'
        iorwf TEMP,0 ;Change bits
	call chgbnk1
        movwf VRCON ;Store result in VRCON
	return
;Program
;==============================
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Deactivate analog comparators
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0
	;Clear voltage reference value
	clrf VALUE
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	bcf TRISB,0 ;Output
	bcf TRISB,1 ;Output
	bcf TRISB,2 ;Output
	bcf TRISB,3 ;Output
	bcf TRISB,4 ;Output
	bsf TRISA,0 ;Input
	bsf TRISA,1 ;Input
	bsf TRISA,2 ;Input (to enable output voltage)
	;Set the voltage reference module
	bsf VRCON,6 ;Output to RA2 pin
	bsf VRCON,5 ;Low range
	bsf VRCON,3 ;1111 value
	bsf VRCON,2
	bsf VRCON,1
	bsf VRCON,0
	bsf VRCON,7 ;Enable voltage reference
Cycle
	;Check buttons
	call chgbnk0
	btfsc PORTA,0 ;Button 0
	goto $+2
	call ChangeVRefRange
	btfsc PORTA,1 ;Button 1
	goto $+2
	call ChangeVRefValue
	;Show current voltage reference state
	call chgbnk1
	btfss VRCON,5 ;Voltage range
	goto $+2
	goto $+4
	call chgbnk0
	bcf PORTB,4
	goto $+3
	call chgbnk0
	bsf PORTB,4
	;Show voltage reference value
	movf PORTB,0 ;Move PORTB to W
	andlw b'11110000' ;Prepare PORTB<0:3> for changing bits
	movwf TEMP
	call chgbnk1
	movf VRCON,0
	andlw b'00001111'
	call chgbnk0
	iorwf TEMP,0 ;Change bits
	movwf PORTB ;Store result in PORTB
	goto Cycle
	end
